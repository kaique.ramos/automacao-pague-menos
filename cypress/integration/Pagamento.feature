Feature: Fluxo de Compra

    Scenario: Fluxo de Compra
        Given acesso a PDP do site Pague Menos
        When clico em comprar
        And clico em item adicionado
        And clico em Avançar no checkout
        And insiro email valido
        And insiro senha valida
        And insiro primeiro nome
        And insiro ultimo nome
        And insiro cpf valido
        And insiro telefone
        And clico em ir para entrega
        And insiro CEP e clico em ok
        And clico em retirar
        And clico em ir para o pagamento
        And clico na opcao Pix
        Then visualizo o texto Pix

