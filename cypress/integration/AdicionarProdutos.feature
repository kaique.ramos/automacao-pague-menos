Feature: Adicionar 2 produtos diferentes e limpar todo o carrinho

    Scenario: Adicionar 2 produtos diferentes
        Given acesso a home do site Pague Menos
        When adiciono um produto
        And adiciono outro produto diferente
        And clico em minha cesta
        And clico em remover primeiro produto
        And clico em remover segundo produto
        Then devo visualizar a mensagem de cesta vazia