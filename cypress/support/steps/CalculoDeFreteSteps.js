/* global Given, And, Then, When */

import CalculoDeFretePage from '../pageobjects/CalculoDeFretePage'
const calculoDeFretePage = new CalculoDeFretePage

Given("acesso a PDP do site Pague Menos", () => {
    calculoDeFretePage.acessarSite();
})

When("insiro o CEP", () => {
    calculoDeFretePage.inserirCEPValido("05832190");
})

And("clico em calcular", () => {
    calculoDeFretePage.clicarEmCalcular();
})

Then("devo visualizar as informações de entrega", () => {
    calculoDeFretePage.visualizarInformacoesCEP();
})





