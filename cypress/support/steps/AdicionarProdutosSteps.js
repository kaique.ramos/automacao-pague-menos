/* global Given, And,Then, When */

import AdicionarProdutosPage from '../pageobjects/AdicionarProdutosPage'
const adicionarProdutosPage = new AdicionarProdutosPage

Given("acesso a home do site Pague Menos", () => {
    adicionarProdutosPage.acessarSite();
})

When("adiciono um produto", () => {
    adicionarProdutosPage.adicionarProduto1();
})

And("adiciono outro produto diferente", () => {
    adicionarProdutosPage.adicionarProduto2();
})

And("clico em minha cesta", () => {
    adicionarProdutosPage.clicarEmMinhaCesta();
})

And("clico em remover primeiro produto", () => {
    adicionarProdutosPage.removerProduto1();
})

And("clico em remover segundo produto", () => {
    adicionarProdutosPage.removerProduto2();
})

Then("devo visualizar a mensagem de cesta vazia", () => {
    adicionarProdutosPage.visualizarCestaVazia();
})










// And adiciono outro produto diferente
// And clico em minha cesta
// And clico em remover primeiro produto
// And clico em remover segundo produto
// Then devo visualizar a mensagem de cesta vazia