/* global Given, And,Then, When */

import PagamentoPage from '../pageobjects/PagamentoPage'
const pagamentoPage = new PagamentoPage

Given("acesso a PDP do site Pague Menos", () => {
    pagamentoPage.acessarSite();
})

When("clico em comprar", () => {
    pagamentoPage.clicarCamprarProduto();
})


And("clico em item adicionado", () => {
     pagamentoPage.clicarItemAdicionado();
})

And("clico em Avançar no checkout", () => {
    pagamentoPage.clicarAvancar();
})

And("insiro email valido", () => {
    pagamentoPage.inserirEmail("kaique.ramos@acct.global");
})

And("insiro senha valida", () => {
    pagamentoPage.inserirSenha("Laranja22");
    pagamentoPage.clicarContinuar();
})

And("insiro primeiro nome", () => {
    pagamentoPage.inserirPrimeiroNome("Teste");
})

And("insiro ultimo nome", () => {
    pagamentoPage.inserirUltimoNome("Teste");
})

And("insiro cpf valido", () => {
    pagamentoPage.inserirCPFValido("984.929.770-00");
})

And("insiro telefone", () => {
    pagamentoPage.inserirTelefone("1112345678");
})

And("clico em ir para entrega", () => {
    pagamentoPage.clicarIrParaEntrega();
})

And("insiro CEP e clico em ok", () => {
    pagamentoPage.inserirCEP("05832190");
    pagamentoPage.clicarOK();
})

And("clico em retirar", () => {
    pagamentoPage.clicarEmRetirar();
})

And("clico em ir para o pagamento", () => {
    pagamentoPage.clicarIrParaPagamento();
})

And("clico na opcao Pix", () => {
    pagamentoPage.clicarOpcaoPix();
})

Then("visualizo o texto Pix", () => {
    pagamentoPage.visualizarTextoPix();
})







