class PagamentoElements {

  botaoComprar = () => { return '.medicamentoControlado > .vtex-button'}
  botaoMinhaCesta = () => { return '.vtex-minicart-2-x-minicartContainer > .pa4'}
  botaoFinalizarPedido = () => { return '#proceed-to-checkout > .vtex-button__label'}
  botaoAvancar = () => { return '#cart-to-orderform'}
  campoCEP = () => { return '.paguemenos-store-theme-2-x-add-cookies-region-component-input-modal'}
  botaoOK = () => { return '.paguemenos-store-theme-2-x-add-cookies-region-component-button-modal'}
  botaoAceitarTodos = () => { return '#dm876A > .dp-bar-button'}
  itemAdicionado = () => { return '.nt4 > .vtex-button > .vtex-button__label'}
  botaoFecharCEP = () => { return '.paguemenos-store-theme-2-x-add-cookies-region-component-modal-close > div'}
  campoDeEmail = () => { return '.vtex-login-2-x-inputContainerEmail > .vtex-input > .vtex-input-prefix__group > .vtex-styleguide-9-x-input'}
  campoSenha = () => { return '.relative > .vtex-input > .vtex-input-prefix__group > .vtex-styleguide-9-x-input'}
  botaoContinuar = () => { return '.vtex-login-2-x-sendButton > .vtex-button > .vtex-button__label'}
  campoNome = () => { return '#client-first-name'}
  campoUltimoNome = () => { return '#client-last-name'}
  campoCPF = () => { return '#client-document'}
  campoTelefone = () => { return '#client-phone'}
  botaoIrParaEntrega = () => { return '#go-to-shipping'}
  campoCEPCheckout = () => { return '.form-postalCode-custom > #ship-postalCode'}
  botaoOKCheckout = () => { return '.button-postalCode-custom'}
  clicarEmRetirar = () => { return '#shipping-option-pickup-in-point > .shp-method-option-text'}
  botaoIrParaPagamento = () => { return '#btn-go-to-payment'}
  opcaoPix = () => { return '#payment-group-instantPaymentPaymentGroup > .payment-group-item-text'}
  textoPix = () => { return ':nth-child(1) > .payment-pix-step-text'}
}

export default PagamentoElements;



