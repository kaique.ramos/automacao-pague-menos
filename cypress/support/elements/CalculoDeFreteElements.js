class CalculoDeFreteElements {

    campoCEP = () => { return '.vtex-address-form-4-x-input'}
    botaoCalcular = () => { return '.vtex-store-components-3-x-shippingContainer > .vtex-button > .vtex-button__label'}
    informacoesDeEntrega = () => { return '.vtex-store-components-3-x-shippingTableCellDeliveryEstimate'}

}

export default CalculoDeFreteElements;

