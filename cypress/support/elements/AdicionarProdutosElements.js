class AdicionarProdutosElements {

    addPrimeiroProduto = () => { return '.vtex-flex-layout-0-x-flexRowContent--new-home-shelf-1 > :nth-child(1) > :nth-child(1) > :nth-child(2) > .vtex-slider-layout-0-x-sliderLayoutContainer > .vtex-slider-layout-0-x-sliderTrackContainer > [data-testid="slider-track"] > [aria-label="2 of 10"] > .vtex-slider-layout-0-x-slideChildrenContainer > .vtex-product-summary-2-x-container > .vtex-product-summary-2-x-clearLink > .vtex-product-summary-2-x-element > :nth-child(5) > .vtex-flex-layout-0-x-flexRow > .mt0 > .pr0 > .paguemenos-store-theme-2-x-containerAddCartButton > .vtex-button > .vtex-button__label'}
    addSegundoProduto = () => { return '[aria-label="3 of 10"] > .vtex-slider-layout-0-x-slideChildrenContainer > .vtex-product-summary-2-x-container > .vtex-product-summary-2-x-clearLink > .vtex-product-summary-2-x-element > :nth-child(5) > .vtex-flex-layout-0-x-flexRow > .mt0 > .pr0 > .paguemenos-store-theme-2-x-containerAddCartButton > .vtex-button > .vtex-button__label'}
    minhaCesta = () => { return '.vtex-minicart-2-x-minicartContainer > .pa4'}
    removerPrimeiroProduto = () => { return ':nth-child(1) > .vtex-store-components-3-x-container > .vtex-flex-layout-0-x-flexRowContent--list-row-minicart > :nth-child(2) > .vtex-flex-layout-0-x-flexCol > :nth-child(2) > .vtex-flex-layout-0-x-flexRow > .mt0 > :nth-child(2) > .paguemenos-store-theme-2-x-quantitySelectorStepper > .vtex-numeric-stepper-wrapper > .vtex-numeric-stepper-container > .vtex-numeric-stepper__minus-button-container > .vtex-numeric-stepper__minus-button > .vtex-numeric-stepper__minus-button__text'}
    removerSegundoProduto = () => { return '.paguemenos-store-theme-2-x-quantitySelectorStepper > .vtex-numeric-stepper-wrapper > .vtex-numeric-stepper-container > .vtex-numeric-stepper__minus-button-container > .vtex-numeric-stepper__minus-button > .vtex-numeric-stepper__minus-button__text'}
    mensagemCestaVazia = () => { return '.lh-copy > .b'}
    botaoHome = () => { return '.vtex-store-components-3-x-logoImage'}
}

export default AdicionarProdutosElements;
