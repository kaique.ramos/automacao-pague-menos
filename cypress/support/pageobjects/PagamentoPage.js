
/// <reference types="Cypress" />

import PagamentoElements from '../elements/PagamentoElements'
const pagamentoElements = new PagamentoElements
const url = Cypress.config("baseUrl")

class PagamentoPage {

    acessarSite() {
        cy.visit(url)
    }

    clicarCamprarProduto() {
        cy.get(pagamentoElements.botaoFecharCEP()).click({Force: true})
        cy.get(pagamentoElements.botaoComprar()).click({Force: true})
    }

    clicarItemAdicionado(){
        cy.get(pagamentoElements.itemAdicionado()).click({Force: true})
    }

    clicarAvancar(){
        cy.get(pagamentoElements.botaoAvancar()).click({Force: true})
    }

    inserirEmail(email){
        cy.get(pagamentoElements.campoDeEmail()).type(email)
    }

    inserirSenha(senha){
        cy.get(pagamentoElements.campoSenha()).type(senha)
    }

    clicarContinuar(){
        cy.get(pagamentoElements.botaoContinuar()).click({Force: true})
    }

    inserirPrimeiroNome(nome){
        cy.get(pagamentoElements.campoNome()).type(nome)
    }

    inserirUltimoNome(ultimoNome){
        cy.get(pagamentoElements.campoUltimoNome()).type(ultimoNome)
    }

    inserirCPFValido(cpfValido){
        cy.get(pagamentoElements.campoCPF()).type(cpfValido)
    }

    inserirTelefone(telefone){
        cy.get(pagamentoElements.campoTelefone()).type(telefone)
    }

    clicarIrParaEntrega(){
        cy.get(pagamentoElements.botaoIrParaEntrega()).click({Force: true})
    }

    inserirCEP(CEP){
        cy.get(pagamentoElements.campoCEPCheckout()).type(CEP)
    }

    clicarOK(){
        cy.get(pagamentoElements.botaoOKCheckout()).click({Force: true})
    }

    clicarEmRetirar(){
        cy.get(pagamentoElements.clicarEmRetirar()).click({Force: true})
    }

    clicarIrParaPagamento(){
        cy.get(pagamentoElements.botaoIrParaPagamento()).click({Force: true})
    }

    clicarOpcaoPix(){
        cy.get(pagamentoElements.opcaoPix()).click({force: true})
    }

    visualizarTextoPix(){
        cy.get(pagamentoElements.textoPix()).should('exist', 'be.visible')
    }
}

export default PagamentoPage;







