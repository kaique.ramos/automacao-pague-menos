
/// <reference types="Cypress" />

import AdicionarProdutosElements from '../elements/AdicionarProdutosElements'
const adicionarProdutosElements = new AdicionarProdutosElements
const url = Cypress.config("baseUrl")

class AdicionarProdutosPage {

    acessarSite() {
        cy.visit(url)
        cy.get(adicionarProdutosElements.botaoHome()).click({Force: true})
    }

    adicionarProduto1() {
        cy.wait(5000)
        cy.get(adicionarProdutosElements.addPrimeiroProduto()).click({Force: true})
    }

    adicionarProduto2() {
        cy.wait(5000)
        cy.get(adicionarProdutosElements.addSegundoProduto()).click({Force: true})
    }

    clicarEmMinhaCesta() {
        cy.wait(5000)
        cy.get(adicionarProdutosElements.minhaCesta()).click({Force: true})
    }

    removerProduto1() {
        cy.get(adicionarProdutosElements.removerPrimeiroProduto()).click({Force: true})
    }

    removerProduto2() {
        cy.wait(5000)
        cy.get(adicionarProdutosElements.removerPrimeiroProduto()).click({Force: true})
    }

    visualizarCestaVazia() {
        cy.get(adicionarProdutosElements.mensagemCestaVazia()).should('exist', 'be.visible')
    }

}
export default AdicionarProdutosPage;
