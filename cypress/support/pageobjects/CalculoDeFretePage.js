
/// <reference types="Cypress" />

import CalculoDeFreteElements from '../elements/CalculoDeFreteElements'
const calculoDeFreteElements = new CalculoDeFreteElements
const url = Cypress.config("baseUrl")

class CalculoDeFretePage {

    acessarSite() {
        cy.visit(url)
    }

    inserirCEPValido(CEP) {
        cy.wait(15000)
        cy.get(calculoDeFreteElements.campoCEP()).type(CEP,{Force: true})
    }

    clicarEmCalcular() {
        cy.get(calculoDeFreteElements.botaoCalcular()).click({Force: true})
    }

    visualizarInformacoesCEP() {
        cy.get(calculoDeFreteElements.informacoesDeEntrega()).should('exist', 'be.visible')
    }

}

export default CalculoDeFretePage;


